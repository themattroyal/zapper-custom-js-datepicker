/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _sass_styles_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/styles.scss */ \"./src/sass/styles.scss\");\n/* harmony import */ var _sass_styles_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sass_styles_scss__WEBPACK_IMPORTED_MODULE_0__);\n\n\n(function (window) {\n  // Enable strict mode \n  'use strict'; // Function containing our plugin/library code\n\n  function datePickerDIY() {\n    var _datePickerDIY = {};\n    _datePickerDIY.settings = {\n      author: false,\n      date: false\n    };\n\n    _datePickerDIY.init = function (params) {\n      this.settings.date = params.startDate ? new Date(params.startDate) : new Date();\n      this.addPickersDom(this.settings.date);\n      return params;\n    };\n\n    _datePickerDIY.monthDaysMarkup = function (date) {\n      var days = this.allDaysOfMonth(date);\n      var markup = \"\";\n      days.map(function (day) {\n        markup += \"<div class=\\\"day\\\" data-day=\\\"\".concat(day.getDate(), \"\\\">\").concat(day.getDayOfWeek(), \"<br />\").concat(day.getDate(), \"</div>\");\n      }).join('');\n      return markup;\n    };\n\n    _datePickerDIY.pickerMarkup = function (date) {\n      var d = this.dateHelper(date);\n      return \"<div class=\\\"diy-dpicker\\\" data-date=\\\"\".concat(date, \"\\\">\\n\\t\\t\\t\\t<div class=\\\"diy-dhead group\\\">\\n\\t\\t\\t\\t\\t<div class=\\\"month-control\\\">\\n\\t\\t\\t\\t\\t\\t\").concat(this.monthsDropdown(date), \"\\n\\t\\t\\t\\t\\t</div>\\n\\t\\t\\t\\t\\t<div class=\\\"year-control\\\">\\n\\t\\t\\t\\t\\t\\t\\n\\t\\t\\t\\t\\t\\t<a href=\\\"#\\\" class=\\\"year-control prev\\\" data-direction=\\\"previous\\\">\\n\\t\\t\\t\\t\\t\\t\\t<svg class=\\\"svg-icon\\\" viewBox=\\\"0 0 20 20\\\">\\n\\t\\t\\t\\t\\t\\t\\t\\t<path d=\\\"M11.739,13.962c-0.087,0.086-0.199,0.131-0.312,0.131c-0.112,0-0.226-0.045-0.312-0.131l-3.738-3.736c-0.173-0.173-0.173-0.454,0-0.626l3.559-3.562c0.173-0.175,0.454-0.173,0.626,0c0.173,0.172,0.173,0.451,0,0.624l-3.248,3.25l3.425,3.426C11.911,13.511,11.911,13.789,11.739,13.962 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.148,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.148,17.521,10\\\"></path>\\n\\t\\t\\t\\t\\t\\t\\t</svg>\\n\\t\\t\\t\\t\\t\\t</a>\\n\\n\\t\\t\\t\\t\\t\\t<div class=\\\"year-display\\\">\\n\\t\\t\\t\\t\\t\\t\\t\").concat(d.year, \"\\n\\t\\t\\t\\t\\t\\t</div>\\n\\n\\t\\t\\t\\t\\t\\t<a href=\\\"#\\\" class=\\\"year-control next\\\" data-direction=\\\"next\\\">\\n\\t\\t\\t\\t\\t\\t\\t<svg class=\\\"svg-icon\\\" viewBox=\\\"0 0 20 20\\\">\\n\\t\\t\\t\\t\\t\\t\\t\\t<path d=\\\"M12.522,10.4l-3.559,3.562c-0.172,0.173-0.451,0.176-0.625,0c-0.173-0.173-0.173-0.451,0-0.624l3.248-3.25L8.161,6.662c-0.173-0.173-0.173-0.452,0-0.624c0.172-0.175,0.451-0.175,0.624,0l3.738,3.736C12.695,9.947,12.695,10.228,12.522,10.4 M18.406,10c0,4.644-3.764,8.406-8.406,8.406c-4.644,0-8.406-3.763-8.406-8.406S5.356,1.594,10,1.594C14.643,1.594,18.406,5.356,18.406,10M17.521,10c0-4.148-3.374-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.147,17.521,10\\\"></path>\\n\\t\\t\\t\\t\\t\\t\\t</svg>\\n\\t\\t\\t\\t\\t\\t</a>\\n\\n\\t\\t\\t\\t\\t</div>\\n\\t\\t\\t\\t</div>\\n\\t\\t\\t\\t<div class=\\\"diy-dbody\\\">\").concat(this.monthDaysMarkup(date), \"</div>\\n\\t\\t\\t</div>\");\n    };\n\n    _datePickerDIY.monthsDropdown = function (date) {\n      var month = date.getMonth() + 1;\n      var months = {\n        1: 'January',\n        2: 'Febuary',\n        3: 'March',\n        4: 'April',\n        5: 'May',\n        6: 'June',\n        7: 'July',\n        8: 'August',\n        9: 'September',\n        10: 'October',\n        11: 'November',\n        12: 'December'\n      };\n      var options = \"\";\n      Object.keys(months).map(function (key) {\n        var value = months[key];\n        options += \"<option value=\\\"\".concat(key, \"\\\" \").concat(month == key ? 'selected' : '', \">\").concat(value, \"</option>\");\n      });\n      return \"<select class=\\\"month\\\">\".concat(options, \"</select>\");\n    };\n\n    _datePickerDIY.addPickersDom = function (date) {\n      // Add new/update pickers to inputs with .diy-picker class\n      var pickers = document.querySelectorAll('.diy-picker');\n      [].forEach.call(pickers, function (picker) {\n        picker.setAttribute('autocomplete', 'off');\n        picker.insertAdjacentHTML('afterend', this.pickerMarkup(date));\n        picker.addEventListener('focus', this.togglePickerVisibility.bind(this));\n      }.bind(this)); // Add listeners when clicking/selecting a day\n\n      var days = document.querySelectorAll('.day');\n      [].forEach.call(days, function (day) {\n        // Add class to current day\n        var d = this.dateHelper(date);\n        var dd = parseInt(day.getAttribute('data-day'));\n        if (parseInt(d.day) == dd) day.className += ' selected';\n        day.addEventListener('click', function (e) {\n          // Reset all classnames to remove any previously selected classes\n          [].forEach.call(days, function (day) {\n            day.className = 'day';\n          });\n          var picker = e.target.parentElement.parentElement;\n          var date = picker.getAttribute('data-date');\n          var day = e.target.getAttribute('data-day');\n          e.target.className += ' selected';\n          this.updatePickersDay(picker, date, day);\n        }.bind(this));\n      }.bind(this)); // Add listeners for changing month\n\n      var monthPicker = document.querySelectorAll('.month');\n      [].forEach.call(monthPicker, function (month) {\n        month.addEventListener('change', function (e) {\n          var picker = e.target.parentElement.parentElement.parentElement;\n          var date = picker.getAttribute('data-date');\n          var month = e.target.value;\n          this.updatePickersMonth(picker, date, month);\n        }.bind(this));\n      }.bind(this)); // Add listeners for year controls prev/next\n\n      var yearControls = document.querySelectorAll('.year-control');\n      [].forEach.call(yearControls, function (yearControl) {\n        yearControl.addEventListener('click', function (e) {\n          e.stopPropagation();\n          e.preventDefault();\n          var picker = e.target.parentElement.parentElement.parentElement;\n          var date = picker.getAttribute('data-date');\n          var direction = e.target.getAttribute('data-direction');\n          this.updatePickersYear(picker, date, direction);\n        }.bind(this));\n      }.bind(this)); // Hide pickers when clicking out of them (Temporary)\n\n      document.addEventListener('click', function (e) {\n        for (var key in e.target.children) {\n          if (e.target.children.hasOwnProperty(key)) {\n            var div = e.target.children[key];\n            if (div.classList.contains('diy-picker')) return;\n          }\n        }\n\n        if (e.target.closest('.diy-dpicker')) return;\n        var pickers = document.querySelectorAll('.diy-dpicker');\n        [].forEach.call(pickers, function (picker) {\n          picker.style.display = 'none';\n        });\n      });\n      var button = document.getElementById('testSubmitButton');\n      button.addEventListener('click', function (e) {\n        e.preventDefault();\n        console.log('clicked the submit button');\n      });\n    };\n\n    _datePickerDIY.updatePickersDay = function (picker, date, day) {\n      var d = this.dateHelper(date);\n      var dateString = \"\".concat(d.year, \"-\").concat(d.month, \"-\").concat(day);\n      var updatedDate = new Date(dateString); // Update our date selected data property\n\n      picker.setAttribute('data-date-selected', updatedDate); // Update the input field value\n\n      picker.previousSibling.value = dateString; // And we hide the datepicker\n\n      this.hidePicker();\n    };\n\n    _datePickerDIY.updatePickersMonth = function (picker, date, month) {\n      var d = this.dateHelper(date);\n      var updatedDate = new Date(\"\".concat(d.year, \"-\").concat(month, \"-01\"));\n      picker.setAttribute('data-date', updatedDate);\n      picker.querySelector('.diy-dbody').innerHTML = this.monthDaysMarkup(updatedDate); // Update listeners when clicking/selecting a day\n\n      var days = document.querySelectorAll('.day');\n      [].forEach.call(days, function (day) {\n        day.addEventListener('click', function (e) {\n          console.log('test', e); // Reset all classnames to remove any previously selected classes\n\n          [].forEach.call(days, function (day) {\n            day.className = 'day';\n          });\n          var picker = e.target.parentElement.parentElement;\n          var date = picker.getAttribute('data-date');\n          var day = e.target.getAttribute('data-day');\n          e.target.className += ' selected';\n          this.updatePickersDay(picker, date, day);\n        }.bind(this));\n      }.bind(this));\n    };\n\n    _datePickerDIY.updatePickersYear = function (picker, date, direction) {\n      var d = this.dateHelper(date);\n      var updatedYear = direction == 'next' ? d.year + 1 : d.year - 1;\n      var updatedDate = new Date(\"\".concat(updatedYear, \"-\").concat(d.month, \"-01\"));\n      picker.setAttribute('data-date', updatedDate);\n      picker.querySelector('.year-display').innerHTML = updatedYear;\n      picker.querySelector('.diy-dbody').innerHTML = this.monthDaysMarkup(updatedDate); // Update listeners when clicking/selecting a day\n\n      var days = document.querySelectorAll('.day');\n      [].forEach.call(days, function (day) {\n        day.addEventListener('click', function (e) {\n          console.log('test', e); // Reset all classnames to remove any previously selected classes\n\n          [].forEach.call(days, function (day) {\n            day.className = 'day';\n          });\n          var picker = e.target.parentElement.parentElement;\n          var date = picker.getAttribute('data-date');\n          var day = e.target.getAttribute('data-day');\n          e.target.className += ' selected';\n          this.updatePickersDay(picker, date, day);\n        }.bind(this));\n      }.bind(this));\n    };\n\n    _datePickerDIY.dateHelper = function (date) {\n      var d = new Date(date);\n      var year = d.getFullYear();\n      var month = d.getMonth() + 1;\n      var day = d.getDate();\n      return {\n        year: year,\n        month: month <= 9 ? '0' + month : month,\n        day: day <= 9 ? '0' + day : day\n      };\n    };\n\n    _datePickerDIY.togglePickerVisibility = function (e) {\n      // Hide any that may already be open\n      var pickers = document.querySelectorAll('.diy-picker');\n      [].forEach.call(pickers, function (picker) {\n        picker.nextSibling.style.display = 'none';\n      }); // Display the current one\n\n      var picker = e.target.nextSibling;\n      picker.style.display = 'block';\n    };\n\n    _datePickerDIY.hidePicker = function (e) {\n      // Hide any that may already be open\n      var pickers = document.querySelectorAll('.diy-picker');\n      [].forEach.call(pickers, function (picker) {\n        picker.nextSibling.style.display = 'none';\n      });\n    };\n\n    _datePickerDIY.allDaysOfMonth = function (date) {\n      var startDate = date.firstDayOfMonth();\n      var endDate = date.lastDayOfMonth();\n      var days = [];\n\n      while (startDate <= endDate) {\n        days.push(startDate);\n        startDate = startDate.addDay();\n      }\n\n      return days;\n    };\n\n    return _datePickerDIY;\n  } // Make our plugin/library globally accesible by saving it in the window object\n\n\n  if (typeof window.datePickerDIY === 'undefined') {\n    window.datePickerDIY = datePickerDIY();\n  }\n\n  Date.prototype.addDay = function () {\n    var date = new Date(this.valueOf());\n    date.setDate(date.getDate() + 1);\n    return date;\n  };\n\n  Date.prototype.getDayOfWeek = function () {\n    return ['S', 'M', 'T', 'W', 'T', 'F', 'S'][this.getDay()];\n  };\n\n  Date.prototype.firstDayOfMonth = function () {\n    var date = new Date(this.valueOf()),\n        y = date.getFullYear(),\n        m = date.getMonth();\n    return new Date(y, m, 1);\n  };\n\n  Date.prototype.lastDayOfMonth = function () {\n    var date = new Date(this.valueOf()),\n        y = date.getFullYear(),\n        m = date.getMonth();\n    return new Date(y, m + 1, 0);\n  };\n})(window);\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ }),

/***/ "./src/sass/styles.scss":
/*!******************************!*\
  !*** ./src/sass/styles.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/sass/styles.scss?");

/***/ })

/******/ });