import '../sass/styles.scss';

(function(window){

	// Enable strict mode 
	'use strict';

	// Function containing our plugin/library code
	function datePickerDIY()
	{
		let _datePickerDIY = {};

		_datePickerDIY.settings = {
			author 	: false,
			date 	: false
		};

		_datePickerDIY.init = function(params) {
		
			this.settings.date = params.startDate ? new Date(params.startDate) : new Date();

			this.addPickersDom(this.settings.date);

			return params;
		};

		_datePickerDIY.monthDaysMarkup = function(date){

			const days = this.allDaysOfMonth(date);

			let markup = ``;
			days.map((day) => {
				markup += `<div class="day" data-day="${day.getDate()}">${day.getDayOfWeek()}<br />${day.getDate()}</div>`;
			}).join('')

			return markup;
		}

		_datePickerDIY.pickerMarkup = function(date){

			const d = this.dateHelper(date);

			return `<div class="diy-dpicker" data-date="${date}">
				<div class="diy-dhead group">
					<div class="month-control">
						${this.monthsDropdown(date)}
					</div>
					<div class="year-control">
						
						<a href="#" class="year-control prev" data-direction="previous">
							<svg class="svg-icon" viewBox="0 0 20 20">
								<path d="M11.739,13.962c-0.087,0.086-0.199,0.131-0.312,0.131c-0.112,0-0.226-0.045-0.312-0.131l-3.738-3.736c-0.173-0.173-0.173-0.454,0-0.626l3.559-3.562c0.173-0.175,0.454-0.173,0.626,0c0.173,0.172,0.173,0.451,0,0.624l-3.248,3.25l3.425,3.426C11.911,13.511,11.911,13.789,11.739,13.962 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.148,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.148,17.521,10"></path>
							</svg>
						</a>

						<div class="year-display">
							${d.year}
						</div>

						<a href="#" class="year-control next" data-direction="next">
							<svg class="svg-icon" viewBox="0 0 20 20">
								<path d="M12.522,10.4l-3.559,3.562c-0.172,0.173-0.451,0.176-0.625,0c-0.173-0.173-0.173-0.451,0-0.624l3.248-3.25L8.161,6.662c-0.173-0.173-0.173-0.452,0-0.624c0.172-0.175,0.451-0.175,0.624,0l3.738,3.736C12.695,9.947,12.695,10.228,12.522,10.4 M18.406,10c0,4.644-3.764,8.406-8.406,8.406c-4.644,0-8.406-3.763-8.406-8.406S5.356,1.594,10,1.594C14.643,1.594,18.406,5.356,18.406,10M17.521,10c0-4.148-3.374-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.147,3.374,7.521,7.521,7.521C14.147,17.521,17.521,14.147,17.521,10"></path>
							</svg>
						</a>

					</div>
				</div>
				<div class="diy-dbody">${this.monthDaysMarkup(date)}</div>
			</div>`;
		}

		_datePickerDIY.monthsDropdown = function(date){

			const month = date.getMonth()+1;

			const months = {
				1 : 'January', 2 : 'Febuary', 3 : 'March', 4 : 'April', 5 : 'May', 6 : 'June', 
				7 : 'July', 8 : 'August', 9 : 'September', 10 : 'October', 11: 'November', 12: 'December' }

			let options = ``;
			Object.keys(months).map(key => {
				let value = months[key];
				options += `<option value="${key}" ${(month == key ? 'selected' : '')}>${value}</option>`;
			});

			return `<select class="month">${options}</select>`;
		}

		_datePickerDIY.addPickersDom = function(date){

			// Add new/update pickers to inputs with .diy-picker class
			const pickers = document.querySelectorAll('.diy-picker');
			[].forEach.call(pickers, function(picker) {
				picker.setAttribute('autocomplete', 'off');
				picker.insertAdjacentHTML( 'afterend', this.pickerMarkup(date) );
				picker.addEventListener( 'focus', this.togglePickerVisibility.bind(this) );
			}.bind(this));

			// Add listeners when clicking/selecting a day
			const days = document.querySelectorAll('.day');
			[].forEach.call(days, function(day) {

				// Add class to current day
				let d 	= this.dateHelper(date);
				let dd 	= parseInt( day.getAttribute('data-day') );
				if( parseInt(d.day) == dd ) day.className += ' selected';
				
				day.addEventListener( 'click', function(e){

					// Reset all classnames to remove any previously selected classes
					[].forEach.call(days, function(day) {
						day.className = 'day';
					});

					let picker = e.target.parentElement.parentElement;
					let date = picker.getAttribute('data-date');
					let day = e.target.getAttribute('data-day');
					e.target.className += ' selected';
					this.updatePickersDay( picker, date, day );
				}.bind(this) );

			}.bind(this));

			// Add listeners for changing month
			let monthPicker = document.querySelectorAll('.month');
			[].forEach.call(monthPicker, function(month) {
				month.addEventListener( 'change', function(e){
					let picker = e.target.parentElement.parentElement.parentElement;
					let date = picker.getAttribute('data-date');
					let month = e.target.value;
					this.updatePickersMonth( picker, date, month );
				}.bind(this) );
			}.bind(this));

			// Add listeners for year controls prev/next
			let yearControls = document.querySelectorAll('.year-control');
			[].forEach.call(yearControls, function(yearControl) {
				yearControl.addEventListener( 'click', function(e){
					e.stopPropagation(); e.preventDefault();
					let picker = e.target.parentElement.parentElement.parentElement;
					let date = picker.getAttribute('data-date');
					let direction = e.target.getAttribute('data-direction');
					this.updatePickersYear( picker, date, direction);
				}.bind(this) );
			}.bind(this));

			// Hide pickers when clicking out of them (Temporary)
			document.addEventListener( 'click', function(e){

				for (let key in e.target.children) {
					if (e.target.children.hasOwnProperty(key)) {
					  
					  let div = e.target.children[key];
					  
					  if( div.classList.contains('diy-picker') )
					  	return;
					}
				}

				if( e.target.closest('.diy-dpicker') ) return;

				let pickers = document.querySelectorAll('.diy-dpicker');
				[].forEach.call(pickers, function(picker) {
					picker.style.display = 'none';
				});
			});

			let button = document.getElementById('testSubmitButton');
			button.addEventListener( 'click', function(e){
				e.preventDefault();
				console.log('clicked the submit button');
			});
		}

		_datePickerDIY.updatePickersDay = function(picker, date, day){

			const d = this.dateHelper(date);

			const dateString = `${d.year}-${d.month}-${day}`;
			const updatedDate = new Date(dateString);

			// Update our date selected data property
			picker.setAttribute('data-date-selected', updatedDate);			

			// Update the input field value
			picker.previousSibling.value = dateString;

			// And we hide the datepicker
			this.hidePicker();
		}

		_datePickerDIY.updatePickersMonth = function(picker, date, month){

			const d = this.dateHelper(date);

			const updatedDate = new Date(`${d.year}-${month}-01`);

			picker.setAttribute('data-date', updatedDate);
			picker.querySelector('.diy-dbody').innerHTML = this.monthDaysMarkup(updatedDate);

			// Update listeners when clicking/selecting a day
			const days = document.querySelectorAll('.day');
			[].forEach.call(days, function(day) {
				
				day.addEventListener( 'click', function(e){
					console.log('test', e);

					// Reset all classnames to remove any previously selected classes
					[].forEach.call(days, function(day) {
						day.className = 'day';
					});

					let picker = e.target.parentElement.parentElement;
					let date = picker.getAttribute('data-date');
					let day = e.target.getAttribute('data-day');
					e.target.className += ' selected';
					this.updatePickersDay( picker, date, day );
				}.bind(this) );

			}.bind(this));
		}

		_datePickerDIY.updatePickersYear = function(picker, date, direction){
			
			const d = this.dateHelper(date);

			const updatedYear = direction == 'next' ? d.year +1 : d.year -1;
			const updatedDate = new Date(`${updatedYear}-${d.month}-01`);

			picker.setAttribute('data-date', updatedDate);
			picker.querySelector('.year-display').innerHTML = updatedYear;
			picker.querySelector('.diy-dbody').innerHTML = this.monthDaysMarkup(updatedDate);

			// Update listeners when clicking/selecting a day
			const days = document.querySelectorAll('.day');
			[].forEach.call(days, function(day) {
				
				day.addEventListener( 'click', function(e){
					console.log('test', e);

					// Reset all classnames to remove any previously selected classes
					[].forEach.call(days, function(day) {
						day.className = 'day';
					});

					let picker = e.target.parentElement.parentElement;
					let date = picker.getAttribute('data-date');
					let day = e.target.getAttribute('data-day');
					e.target.className += ' selected';
					this.updatePickersDay( picker, date, day );
				}.bind(this) );

			}.bind(this));
		}

		_datePickerDIY.dateHelper = function(date){

			const d = new Date(date);
			const year = d.getFullYear();
			const month = d.getMonth() +1;
			const day = d.getDate();

			return {
				year 	: year,
				month 	: (month <= 9 ? '0' + month : month),
				day 	: (day <= 9 ? '0' + day : day)
			}
		}

		_datePickerDIY.togglePickerVisibility = function(e){

			// Hide any that may already be open
			const pickers = document.querySelectorAll('.diy-picker');
			[].forEach.call(pickers, function(picker) {
				picker.nextSibling.style.display = 'none';
			});

			// Display the current one
			let picker = e.target.nextSibling;
      		picker.style.display = 'block';
		}

		_datePickerDIY.hidePicker = function(e){

			// Hide any that may already be open
			const pickers = document.querySelectorAll('.diy-picker');
			[].forEach.call(pickers, function(picker) {
				picker.nextSibling.style.display = 'none';
			});
		}

		_datePickerDIY.allDaysOfMonth = function(date){	

			let startDate = date.firstDayOfMonth();
			let endDate = date.lastDayOfMonth();
			let days = [];

			while (startDate <= endDate) {
				days.push(startDate);
				startDate = startDate.addDay();
			}

			return days;
		};

		return _datePickerDIY;
	}

	// Make our plugin/library globally accesible by saving it in the window object
	if( typeof(window.datePickerDIY) === 'undefined' ){
		window.datePickerDIY = datePickerDIY();
	}

	Date.prototype.addDay = function() {
       let date = new Date(this.valueOf());
       date.setDate(date.getDate() +1);
       return date;
	}
	 
	Date.prototype.getDayOfWeek = function() {   
	    return [ 'S','M','T','W','T','F','S' ][ this.getDay() ];
	};


	Date.prototype.firstDayOfMonth = function() {
		let date = new Date( this.valueOf() ), y = date.getFullYear(), m = date.getMonth();
		return new Date(y, m, 1);
	}

	Date.prototype.lastDayOfMonth = function() {
		let date = new Date( this.valueOf() ), y = date.getFullYear(), m = date.getMonth();
		return new Date(y, m + 1, 0);
	}

})(window);